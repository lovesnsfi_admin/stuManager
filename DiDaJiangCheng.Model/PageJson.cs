﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiDaJiangCheng.Model
{
    [Serializable]
    public class PageJson
    {
        public string status { get; set; }
        public string msg { get; set; }
        public object data { get; set; }

        public PageJson(string status,string msg)
        {
            this.status = status;
            this.msg = msg;
            this.data = null;
        }
        public PageJson(string status, string msg, object data)
        {
            this.status = status;
            this.msg = msg;
            this.data = data;
        }
    }
}
