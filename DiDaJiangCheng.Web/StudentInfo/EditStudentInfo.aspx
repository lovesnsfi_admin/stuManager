﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditStudentInfo.aspx.cs" Inherits="DiDaJiangCheng.Web.StudentInfo.EditStudentInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="../css/bootstrapV3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../js/layer/theme/default/layer.css" rel="stylesheet" />
    <!--[if lt ie 9]>
        <script src="../css/bootstrapV3/html5shiv.min.js" type="text/javascript"></script>
        <script src="../css/bootstrapV3/respond.min.js" type="text/javascript"></script>
    <![endif]-->
    <title>编辑学生</title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <div class="page-header">
            <h2 class="text-center text-primary">编辑学生</h2>
        </div>
        <div class="form-horizontal" role="form">
            <div class="form-group">
                <label class="control-label col-xs-2">学生学号</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" placeholder="请输入学号" v-model="formData.StuNo" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-2">学生姓名</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" placeholder="请输入姓名" v-model="formData.StuName"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label">学生头像</label>
                <div class="col-xs-7">
                    <input type="file" ref="StuPhotoFile" @change="StuPhotoChange($event)"  style="display:none" />
                    <img  src="../imgs/upload.png" class="img-responsive img-thumbnail" style="cursor:pointer;max-width:100px" ref="imgView" @click="$refs.StuPhotoFile.click()" />
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-2">学生性别</label>
                <div class="col-xs-7">
                    <select class="form-control" v-model="formData.StuSex">
                        <option value="男">男</option>
                        <option value="女">女</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label">学生民族</label>
                <div class="col-xs-7">
                    <select class="form-control" v-model="formData.StuNation">
                        <option v-for="(item,index) in nationList " :key="index" :value="item" v-text="item"></option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label" >联系方式</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" placeholder="请输入联系方式"  v-model="formData.StuContact"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label">邮政编码</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" placeholder="请输入邮政编码" v-model="formData.StuPostCode"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label">政治面貌</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" placeholder="请输入政治面貌" v-model="formData.Stupolitics"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label">班级编号</label>
                <div class="col-xs-7">
                    <select class="form-control" v-model="formData.ClassId">
                        <option v-for="(item,index) in allClassList" :key="index" :value="item.ClassId" v-text="item.ClassName"></option>
                    </select>
                </div>
            </div>
           <div class="form-group">
                <label class="col-xs-2 control-label">家庭地址</label>
                <div class="col-xs-7">
                    <textarea class="form-control" placeholder="请输入家庭地址" style="height:150px" v-model="formData.StuFamilyAddress"></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-7 col-xs-offset-2">
                    <button type="button" class="btn btn-primary" :disabled="!isReady" @click="update">
                        <span class="glyphicon glyphicon-save"></span>保存
                    </button>
                    <button type="button" class="btn btn-danger" @click="location.href='./StudentInfoList.aspx'">
                        <span class="glyphicon glyphicon-arrow-left"></span>返回列表 
                    </button>
                </div>
            </div>
        </div>
    </div>
    </form>
    <script src="../css/bootstrapV3/js/jquery.min.js" type="text/javascript"></script>
    <script src="../css/bootstrapV3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/layer/layer.js" type="text/javascript"></script>
    <script src="../js/vue.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        new Vue({
            el: "#form1",
            data: {
                nationList: ["汉族", "满族", "内蒙古", "回族", "藏族", "其它"],
                allClassList: [],
                formData: {
                    StuId:"",
                    StuNo: "",
                    StuName: "",
                    StuSex: "男",
                    StuNation: "汉族",
                    StuFamilyAddress: "",
                    StuContact: "",
                    Stupolitics: "",
                    StuPostCode: "",
                    ClassId: ""
                },
                StuPhoto: ""
            },
            computed: {
                isReady: function () {
                    var flag = true;
                    for (var i in this.formData) {
                        if (this.formData[i] == null || this.formData[i] == "" || this.formData[i] == undefined) {
                            flag = false;
                            break;
                        }
                    }
                    return flag;
                }
            },
            created: function () {
                this.getAllClassList();
                var u = new URL(location.href);
                if (u.searchParams.get("type")=="edit") {
                    //说明是编辑
                    this.formData.StuId = u.searchParams.get("stuId");
                    //根据stuId去获取数据
                    this.getModel(this.formData.StuId);
                }
            },
            mounted:function(){
                
            },
            methods: {
                StuPhotoChange: function (event) {
                    var file = event.currentTarget.files[0];
                    var reg = /^^(image)\/\w{3,4}$/;
                    var that = this;
                    if (reg.test(file.type)) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            that.$refs.imgView.src = e.target.result;
                            that.StuPhoto = e.target.result;
                        }
                        reader.readAsDataURL(file);
                    }
                    else {
                        layer.alert("文件类型不正确，请选择图片");
                    }
                },
                getAllClassList: function () {
                    var jsonStr = StudentInfo.getAllClassList();
                    var jsonObj = JSON.parse(jsonStr.value);
                    if (jsonObj.status == "success") {
                        this.allClassList = jsonObj.data;
                        this.formData.ClassId = this.allClassList[0].ClassId;
                    }
                    else {
                        layer.alert("获取班级数据失败");
                    }
                },
                getModel:function(stuId){
                    var jsonStr = StudentInfo.getModel(stuId);
                    var jsonObj = JSON.parse(jsonStr.value);
                    var that = this;
                    if (jsonObj.status=="success") {
                        for (var i in this.formData) {
                            for (var j in jsonObj.data) {
                                if (i==j) {
                                    this.formData[i] = jsonObj.data[j];
                                }
                            }
                        }
                        this.StuPhoto = jsonObj.data["StuPhoto"];
                        //修正图片格式
                        this.$nextTick(function () {
                            this.StuPhoto = "data:image/jpeg;base64," + this.StuPhoto;
                            that.$refs.imgView.src = this.StuPhoto;
                        })
                    }
                    else {
                        layer.alert("获取数据失败", {}, function () {
                            layer.closeAll();
                        })
                    }
                },
                update: function () {
                    var index = layer.load(0, { shade: false });
                    var jsonStr = StudentInfo.update(JSON.stringify(this.formData), this.StuPhoto.split("base64,")[1]);
                    console.log(jsonStr);
                    var jsonObj = JSON.parse(jsonStr.value);
                    layer.close(index);
                    if (jsonObj.status=="success") {
                        layer.alert("修改成功");
                    }
                    else {
                        layer.alert("修改失败，请重试或联系管理员");
                    }
                 
                }
            }
        })

    </script>
</body>
</html>
