﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;

namespace DiDaJiangCheng.Web.StudentInfo
{
    [AjaxNamespace("StudentInfo")]
    public partial class EditStudentInfo : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(EditStudentInfo));
        }
        [AjaxMethod]
        public string getAllClassList()
        {
            DataSet ds = new BLL.tbClassInfo().GetAllList();
            return JsonConvert.SerializeObject(new Model.PageJson("success", "获取班级成功", ds.Tables[0]));
        }

        [AjaxMethod]
        public string getModel(int StuId)
        {
            Model.tbStudentInfo model = new BLL.tbStudentInfo().GetModel(StuId);
            if (model!=null)
            {
                return JsonConvert.SerializeObject(new Model.PageJson("success", "获取数据成功", model));
            }
            else
            {
                return JsonConvert.SerializeObject(new Model.PageJson("fail", "获取数据失败"));
            }
        }

        [AjaxMethod]
        public string update(string modelStr, string base64)
        {
            Model.tbStudentInfo model = JsonConvert.DeserializeObject<Model.tbStudentInfo>(modelStr);
            model.StuPhoto = Convert.FromBase64String(base64);
            bool result = new BLL.tbStudentInfo().Update(model);
            if (result)
            {
                return JsonConvert.SerializeObject(new Model.PageJson("success", "修改成功"));
            }
            else
            {
                return JsonConvert.SerializeObject(new Model.PageJson("fail", "修改失败"));
            }
        }
    }
}