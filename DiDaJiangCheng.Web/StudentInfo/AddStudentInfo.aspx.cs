﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace DiDaJiangCheng.Web.StudentInfo
{
    [AjaxNamespace("StudentInfo")]
    public partial class AddStudentInfo : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(AddStudentInfo));
        }

        [AjaxMethod]
        public string getAllClassList()
        {
            DataSet ds = new BLL.tbClassInfo().GetAllList();
            return JsonConvert.SerializeObject(new Model.PageJson("success", "获取班级成功", ds.Tables[0]));
        }

        [AjaxMethod]
        public string add(string modelStr,string base64)
        {
            //反序列化
            Model.tbStudentInfo model = JsonConvert.DeserializeObject<Model.tbStudentInfo>(modelStr);

            byte[] buffer = Convert.FromBase64String(base64);
            model.StuPhoto = buffer;
            int i = new BLL.tbStudentInfo().Add(model);
            if (i>0)
            {
                return JsonConvert.SerializeObject(new Model.PageJson("success", "新增学生信息成功"));
            }
            else
            {
                return JsonConvert.SerializeObject(new Model.PageJson("fail", "新增学生信息失败"));
            }
        }
    }
}