﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentInfoList.aspx.cs" Inherits="DiDaJiangCheng.Web.StudentInfo.StudentInfoList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="../css/bootstrapV3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../js/layer/theme/default/layer.css" rel="stylesheet" />
    <!--[if lt ie 9]>
        <script src="../css/bootstrapV3/html5shiv.min.js" type="text/javascript"></script>
        <script src="../css/bootstrapV3/respond.min.js" type="text/javascript"></script>
    <![endif]-->
    <title>班级列表</title>
</head>
<body>
    <form id="form1" runat="server">
    <div class=" container-fluid">
        <div class="page-header">
            <h2 class="text-center text-primary">班级列表</h2>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-primary" @click="location.href='./AddStudentInfo.aspx'">
                <span class="glyphicon glyphicon-plus"></span>新增
            </button>
            <button type="button" class="btn btn-warning" @click="editStudent">
                <span class="glyphicon glyphicon-pencil"></span>修改
            </button>
            <button type="button" class="btn btn-danger">
                <span class="glyphicon glyphicon-remove"></span>删除
            </button>
        </div>
        <div role="form" class="form-inline" style="margin:10px 0">
            <div class="form-group">
                <label class="control-label">学生学号</label>
                <input type="text" class="form-control" placeholder="按学号查询" v-model="StuNo"/>
            </div>
            <div class="form-group">
                <label class="control-label">学生姓名</label>
                <input type="text" class="form-control" placeholder="按姓名查询" v-model="StuName" />
            </div>
            <div class="form-group">
                <label class="control-label">性别</label>
                <select class="form-control" v-model="StuSex">
                    <option value="0">-请选择-</option>
                    <option value="男">男</option>
                    <option value="女">女</option>
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">民族</label>
                <select class="form-control" v-model="StuNation">
                    <option value="0">-请选择-</option>
                    <option v-for="(item,index) in nationList" :key="index" :value="item" v-text="item"></option>
                </select>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-primary" @click="getList">
                    <span class="glyphicon glyphicon-search"></span>查询
                 </button>
            </div>
            
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-striped table-bordered">
                <tr>
                    <th class="col-xs-1">
                        <input type="checkbox" id="ckAll" ref="ckAll" v-model="isCheckedAll" />
                        <label for="ckAll">全选</label>
                    </th>
                    <th>学生学号</th>
                    <th>学生姓名</th>
                    <th>性别</th>
                    <th>民族</th>
                    <th>家庭地址</th>
                    <th>联系方式</th>
                    <th>政治面貌</th>
                    <th>邮政编码</th>
                    <th>班级名称</th>
                    <th>专业名称</th>
                </tr>        
                <tr v-for="(item,index) in StuList" :key="index">
                    <td>
                        <input name="ck" type="checkbox" :value="item.StuId" :checked="isCheckedAll" />
                    </td>
                    <td v-text="item.StuNo"></td>
                    <td v-text="item.StuName"></td>
                    <td v-text="item.StuSex"></td>
                    <td v-text="item.StuNation"></td>
                    <td v-text="item.StuFamilyAddress"></td>
                    <td v-text="item.StuContact"></td>
                    <td v-text="item.Stupolitics"></td>
                    <td v-text="item.StuPostCode"></td>
                    <td v-text="item.ClassName"></td>
                    <td v-text="item.SpecialtyName"></td>
                </tr>    
            </table>
        </div>
        <ul class="pagination pull-right">
            <li class="active"><a href="#">1</a></li>
        </ul>
    </div>
    </form>
    <script src="../css/bootstrapV3/js/jquery.min.js" type="text/javascript"></script>
    <script src="../css/bootstrapV3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/layer/layer.js" type="text/javascript"></script>
    <script src="../js/vue.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        new Vue({
            el: "#form1",
            data: {
                StuNo: "",
                StuName: "",
                StuSex: "0",
                StuNation:"0",
                nationList: ["汉族", "满族", "内蒙古", "回族", "藏族", "其它"],
                StuList: [],
                isCheckedAll:false     //是否全选
            },
            computed: {
               
            },
            created: function () {
                this.getList();
            },
            methods: {
                //查询
                getList: function () {
                    var index = layer.load(0, { shade: false });
                    var jsonStr = StudentInfoList.getList(this.StuNo, this.StuName, this.StuSex, this.StuNation);
                    var jsonObj = JSON.parse(jsonStr.value);
                    if (jsonObj.status=="success") {
                        this.StuList = jsonObj.data;
                    }
                    layer.close(index);
                },
                //编辑学生按钮事件
                editStudent: function () {
                    var length = $("input[name='ck']:checked").length;
                    if (length==1) {
                        var currentStuId = $("input[name='ck']:checked").val();
                        var index = layer.open({
                            type: 2,
                            content: "./EditStudentInfo.aspx?stuId=" + currentStuId + "&type=edit"
                        });
                        layer.full(index);
                    }
                    else if (length<1) {
                        layer.alert("请选择需要编辑的数据");
                    }
                    else if (length>1) {
                        layer.alert("一次只能编辑一条数据");
                    }
                }
                
            }
        })
    </script>
</body>
</html>
