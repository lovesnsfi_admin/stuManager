﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using System.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Diagnostics;

namespace DiDaJiangCheng.Web.StudentInfo
{
    [AjaxNamespace("StudentInfoList")]
    public partial class StudentInfoList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(StudentInfoList));
        }

        [AjaxMethod]
        public string getList(string StuNo, string StuName, string StuSex, string StuNation)
        {
            string strWhere = " 1=1 ";
            if (!string.IsNullOrEmpty(StuNo))
            {
                strWhere += " and StuNo like '%" + StuNo + "%'";
            }
            if (!string.IsNullOrEmpty(StuName))
            {
                strWhere += " and StuName like '%" + StuName + "%'";
            }
            if (StuSex != "0")
            {
                strWhere += " and StuSex ='" + StuSex + "'";
            }
            if (StuNation != "0")
            {
                strWhere += " and StuNation='" + StuNation + "'";
            }
            Debug.WriteLine("----------------");
            Debug.WriteLine(strWhere);
            DataSet ds = new BLL.tbStudentInfo().GetListByStudentInfoView(strWhere);
            return JsonConvert.SerializeObject(new Model.PageJson("success", "获取数据成功", ds.Tables[0]));
        }


    }

}