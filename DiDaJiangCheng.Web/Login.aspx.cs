﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DiDaJiangCheng.Model;
using System.Data;
using AjaxPro;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace DiDaJiangCheng.Web
{
    [AjaxPro.AjaxNamespace("MyAjax")]
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(Login));
        }

        [AjaxPro.AjaxMethod]
        public string checkLogin(string userName,string pwd)
        {
            Model.tbUserInfo model = new BLL.tbUserInfo().checkLogin(userName, pwd);
            if (model!=null)
            {
                Session["userInfo"] = model;
                return JsonConvert.SerializeObject(new PageJson("success", "登陆成功", model));
            }
            else
            {
                return JsonConvert.SerializeObject(new PageJson("fail", "登陆失败，用户名与密码不正确", model));
            }
        }
    }
}