﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ClassInfoList.aspx.cs" Inherits="DiDaJiangCheng.Web.ClassInfo.ClassInfoList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="../css/bootstrapV3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../js/layer/theme/default/layer.css" rel="stylesheet" />
    <!--[if lt ie 9]>
        <script src="../css/bootstrapV3/html5shiv.min.js" type="text/javascript"></script>
        <script src="../css/bootstrapV3/respond.min.js" type="text/javascript"></script>
    <![endif]-->
    <title>班级列表</title>
</head>
<body>
    <form id="form1" runat="server">
    <div class=" container-fluid">
        <div class="page-header">
            <h2 class="text-center text-primary">班级列表</h2>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-primary" @click="location.href='./AddClassInfo.aspx'">
                <span class="glyphicon glyphicon-plus"></span>新增
            </button>
            <button type="button" class="btn btn-warning" @click="edit">
                <span class="glyphicon glyphicon-pencil"></span>修改
            </button>
            <button type="button" class="btn btn-danger">
                <span class="glyphicon glyphicon-remove"></span>删除
            </button>
        </div>
        <div role="form" class="form-inline" style="margin:5px 0">
            <div class="form-group">
                <label class="control-label">班级名称</label>
                <input type="text" v-model="ClassName" class="form-control" placeholder="班级名称查询"  />
            </div>
            <div class="form-group">
                <label class="control-label">专业名称</label>
                <select class="form-control" v-model="SpecialtyId">
                    <option value="0">-请选择-</option>
                    <option v-for="(item,index) in appSpecialtyList" :key="index" :value="item.SpecialtyId">{{item.SpecialtyName}}</option>
                </select>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-primary" @click="queryData">
                    <span class="glyphicon glyphicon-search"></span>查询
                </button>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-striped table-bordered">
                <tr>
                    <th class="col-xs-1">
                        <input type="checkbox" id="ckAll" ref="ckAll" v-model="isCheckedAll"/>
                        <label for="ckAll">全选</label>
                    </th>
                    <th class="col-xs-1">班级ID</th>
                    <th class="col-xs-1">班级名称</th>
                    <th class="col-xs-1">专业名称</th>
                    <th class="col-xs-1">教室编号</th>
                    <th class="col-xs-1">学制</th>
                    <th class="col-xs-2">入学日期</th>
                    <th class="col-xs-3">备注</th>
                </tr>
                <tr v-for="(item,index) in classInfoList" :key="index">
                    <td>
                        <input type="checkbox" name="ck" :value="item.ClassId" :checked="isCheckedAll" />
                    </td>
                    <td>{{item.ClassId}}</td>
                    <td>{{item.ClassName}}</td>
                    <td>{{item.SpecialtyName}}</td>
                    <td>{{item.classroomId}}</td>
                    <td>{{item.eductionalSystem}}</td>
                    <td>{{item.enterTime|formatDate}}</td>
                    <td>{{item.Marks}}</td>
                    
                </tr>
            </table>
        </div>
        <ul class="pagination pull-right">
            <li class="active"><a href="#">1</a></li>
        </ul>
    </div>
    </form>
    <script src="../css/bootstrapV3/js/jquery.min.js" type="text/javascript"></script>
    <script src="../css/bootstrapV3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/layer/layer.js" type="text/javascript"></script>
    <script src="../js/vue.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        new Vue({
            el: "#form1",
            data: {
                ClassName: "",
                SpecialtyId:"0",
                classInfoList: [],
                isCheckedAll: false
            },
            computed:{
                appSpecialtyList: function () {
                    var jsonStr = ClassInfoList.getAllSpecialtyList();
                    var jsonObj = JSON.parse(jsonStr.value);
                    return jsonObj.data;
                },
            },
            created: function () {
                this.getAllClassInfoList();
            },
            methods: {
                getAllClassInfoList: function () {
                    var index = layer.load(0, { shade: false }); //0代表加载的风格，支持0-2
                    var jsonStr = ClassInfoList.getClassInfoList("","0");
                    var jsonObj = JSON.parse(jsonStr.value);
                    this.classInfoList = jsonObj.data;
                    layer.close(index);
                },
                //查询数据
                queryData: function () {
                    var index = layer.load(0, { shade: false }); //0代表加载的风格，支持0-2
                    var jsonStr = ClassInfoList.getClassInfoList(this.ClassName, new String(this.SpecialtyId).valueOf());
                    console.log(jsonStr);
                    var jsonObj = JSON.parse(jsonStr.value);
                    this.classInfoList = jsonObj.data;
                    layer.close(index);
                },
                //编辑数据
                edit: function () {
                    var length = $("input[name='ck']:checked").length;
                    if (length==1) {
                        var currentClassId = $("input[name='ck']:checked").val();
                        var index = layer.open({
                            type: 2,
                            content: "./AddClassInfo.aspx?classId="+currentClassId+"&type=edit"
                        });
                        layer.full(index);

                    }
                    else if (length<1) {
                        layer.alert("请选择你要编辑的项");
                    }
                    else if (length>1) {
                        layer.alert("您一次只能编辑一条数据");
                    }
                }
            },
            filters: {
                formatDate: function (dateStr) {
                    var d = new Date(dateStr);
                    var year = d.getFullYear();
                    var month = d.getMonth() + 1;
                    var date = d.getDate();
                    return [year, month, date].join("-");
                }
            }
        })
    </script>
</body>
</html>
