﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Diagnostics;
namespace DiDaJiangCheng.Web.ClassInfo
{
    [AjaxNamespace("ClassInfo")]
    public partial class AddClassInfo : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(AddClassInfo));
        }

        [AjaxMethod]
        public string getAllSpecialtyInfoList()
        {
           DataSet ds = new BLL.tbSpecialtyInfo().GetAllList();
           return JsonConvert.SerializeObject(new Model.PageJson("success", "获取专业信息成功", ds.Tables[0]));
        }

        [AjaxMethod]
        public string Add(string jsonModel)
        {
            Model.tbClassInfo model = JsonConvert.DeserializeObject<Model.tbClassInfo>(jsonModel);
            int i = new BLL.tbClassInfo().Add(model);
            if (i>0)
            {
                return JsonConvert.SerializeObject(new Model.PageJson("success", "新增成功"));
            }
            else
            {
                return JsonConvert.SerializeObject(new Model.PageJson("fail", "新增失败"));
            }
        }
        [AjaxMethod]
        public string getModel(int classId)
        {
            Model.tbClassInfo model = new BLL.tbClassInfo().GetModel(classId);
            if (model!=null)
            {
                return JsonConvert.SerializeObject(new Model.PageJson("success", "获取数据成功", model), base.jsonSettings);

            }
            else
            {
                return JsonConvert.SerializeObject(new Model.PageJson("fail", "数据不存在"));
            }
        }

        [AjaxMethod]
        public string update(int classId,string jsonModel)
        {
            Model.tbClassInfo model = JsonConvert.DeserializeObject<Model.tbClassInfo>(jsonModel);
            model.ClassId = classId;
           bool flag = new BLL.tbClassInfo().Update(model);
           if (flag)
           {
               return JsonConvert.SerializeObject(new Model.PageJson("success", "修改成功"));
           }
           else
           {
               return JsonConvert.SerializeObject(new Model.PageJson("fail", "修改失败"));
           }
        }
    }

    
}