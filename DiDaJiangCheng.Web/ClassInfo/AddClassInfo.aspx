﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddClassInfo.aspx.cs" Inherits="DiDaJiangCheng.Web.ClassInfo.AddClassInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="../css/bootstrapV3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../js/layer/theme/default/layer.css" rel="stylesheet" />
    <link href="../js/My97DatePicker/skin/default/datepicker.css" rel="stylesheet" />
    <!--[if lt ie 9]>
        <script src="../css/bootstrapV3/html5shiv.min.js" type="text/javascript"></script>
        <script src="../css/bootstrapV3/respond.min.js" type="text/javascript"></script>
    <![endif]-->
    <title>新增班级</title>
</head>
<body>
    <form id="form1" runat="server">
     <div class="container">
         <div class="page-header">
             <h2 class="text-center text-primary">新增班级</h2>
         </div>
         <div role="form" class="form-horizontal">
             <div class="form-group">
                 <label class="control-label col-xs-2">班级名称</label>
                 <div class="col-xs-7">
                     <input type="text" name="ClassName" class="form-control" v-model="formData.ClassName" placeholder="请输入班级名称"/>
                 </div>
             </div>
             <div class="form-group">
                 <label class="control-label col-xs-2">专业名称</label>
                 <div class="col-xs-7">
                     <select name="SpecialtyId" class="form-control" v-model="formData.SpecialtyId">
                         <option value="">-请选择-</option> 
                         <option v-for="(item,index) in AllClassInfoList" :value="item.SpecialtyId" :key="index">{{item.SpecialtyName}}</option>
                     </select>
                 </div>
             </div>
             <div class="form-group">
                 <label class="control-label col-xs-2">教室编号</label>
                 <div class="col-xs-7">
                     <input type="text" name="classroomId" v-model="formData.classroomId" class="form-control" />
                 </div>
             </div>
             <div class="form-group">
                 <label class="control-label col-xs-2">学制</label>
                 <div class="col-xs-7">
                     <input type="text" name="eductionalSystem" v-model="formData.eductionalSystem" class="form-control" />
                 </div>
             </div>
            <div class="form-group">
                 <label class="control-label col-xs-2">入学时间</label>
                 <div class="col-xs-7">
                     <input type="text" readonly onclick="WdatePicker({ onpicking: function (dp) { vm.$data.formData.enterTime = dp.cal.getNewDateStr(); } })" name="enterTime" v-model="formData.enterTime" class="form-control"/>
                 </div>
             </div>
             <div class="form-group">
                 <label class="col-xs-2 control-label">备注</label>
                 <div class="col-xs-7">
                     <textarea name="Marks" class="form-control" v-model="formData.Marks" placeholder="请输入备注信息" style="height:150px"></textarea>
                 </div>
             </div>
             <div class="form-group">
                 <div class="col-xs-7 col-xs-offset-2">
                     <button v-on:click="saveClassInfo" type="button" class="btn btn-primary" :disabled="!isReady">
                         <span class="glyphicon glyphicon-save"></span>保存
                     </button>
                     <button type="button" class="btn btn-danger" v-on:click="location.href='./ClassInfoList.aspx'">
                         <span class="glyphicon glyphicon-arrow-left"></span>返回列表
                     </button>
                 </div>
             </div>
         </div>
     </div>
    </form>
    <script src="../css/bootstrapV3/js/jquery.min.js" type="text/javascript"></script>
    <script src="../css/bootstrapV3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/layer/layer.js" type="text/javascript"></script>
    <script src="../js/My97DatePicker/WdatePicker.js"></script>
    <script src="../js/vue.min.js" type="text/javascript"></script>
    <script type="text/javascript">
       var vm = new Vue({
            el: "#form1",
            data: {
                classId:"",
                formData: {
                    ClassName: "",
                    SpecialtyId: "",
                    classroomId: "",
                    eductionalSystem: "",
                    enterTime: "",
                    Marks:""
                },
                AllClassInfoList:[]
            },
            computed:{
                isReady: function () {
                    var flag = true;
                    for (var i in this.formData) {
                        if (this.formData[i] == null || this.formData[i] == "" || this.formData[i] == undefined) {
                            flag = false;
                            break;
                        }
                    }
                    return flag;
                }
            },
            created: function () {
                var json = ClassInfo.getAllSpecialtyInfoList();
                var jsonObj = JSON.parse(json.value);
                if (jsonObj.status == "success") {
                    this.AllClassInfoList = jsonObj.data;
                }
                var u = new URL(location.href);
                if (u.searchParams.get("type")=="edit") {
                    //说明是编辑
                    this.classId = u.searchParams.get("classId");
                    var modelStr = ClassInfo.getModel(parseInt(this.classId));
                    var modelObj = JSON.parse(modelStr.value);
                    if (modelObj.status=="success") {
                        //动态赋值 
                        for (var i in modelObj.data) {
                            for (var j in this.formData) {
                                if (i == j) {
                                    this.formData[j] = modelObj.data[i];
                                }
                            }
                        }
                    }
                    
                }
            },
            methods: {
                saveClassInfo: function () {
                    var index = layer.load(0, { shade: false }); //0代表加载的风格，支持0-2
                    if (this.classId=="") {
                        //新增
                        var json = ClassInfo.Add(JSON.stringify(this.formData));
                        var jsonObj = JSON.parse(json.value);
                        layer.close(index);
                        if (jsonObj.status == "success") {
                            layer.alert("新增成功", {}, function () {
                                location.href = "./ClassInfoList.aspx";
                            });
                        }
                        else {
                            layer.alert("新增失败");
                        }
                    }
                    else {
                        //修改
                        var json = ClassInfo.update(parseInt(this.classId), JSON.stringify(this.formData));
                        var jsonObj = JSON.parse(json.value);
                        layer.close(index);
                        if (jsonObj.status == "success") {
                            layer.alert("编辑成功");
                        }
                        else {
                            layer.alert("编辑失败");
                        }
                        
                    }
                    
                }
            }
        })

    </script>
</body>
</html>
