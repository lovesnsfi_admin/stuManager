﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxPro;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Diagnostics;

namespace DiDaJiangCheng.Web.ClassInfo
{
    [AjaxNamespace("ClassInfoList")]
    public partial class ClassInfoList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AjaxPro.Utility.RegisterTypeForAjax(typeof(ClassInfoList));
            
        }
        [AjaxMethod]
        public string getAllSpecialtyList()
        {
           DataSet ds =  new BLL.tbSpecialtyInfo().GetAllList();
           return JsonConvert.SerializeObject(new Model.PageJson("success", "获取所有班级成功", ds.Tables[0]));
        }

        [AjaxMethod]
        public string getClassInfoList(string className, string specialtyId)
        {
            
            
            string strWhere = " 1=1 ";
            if (!string.IsNullOrEmpty(className))
            {
                strWhere += " and ClassName like '%" + className + "%'";
            }
            if (specialtyId != "0")
            {
                strWhere += " and SpecialtyId ='" + specialtyId + "'";
            }
            Debug.WriteLine(strWhere);
            DataSet ds = new BLL.tbClassInfo().GetListByClassInfoView(strWhere);
            return JsonConvert.SerializeObject(new Model.PageJson("success", "获取数据成功", ds.Tables[0]), this.jsonSettings);
            
        }
    }
}