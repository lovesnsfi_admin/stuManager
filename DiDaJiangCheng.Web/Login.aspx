﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DiDaJiangCheng.Web.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>登陆教学管理系统</title>
    <link href="css/bootstrapV3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="js/layer/theme/default/layer.css" rel="stylesheet" />
    <!--[if lt ie 9]>
        <script src="css/bootstrapV3/html5shiv.min.js" type="text/javascript"></script>
        <script src="css/bootstrapV3/respond.min.js" type="text/javascript"></script>
    <![endif]-->
    <style type="text/css">
        body{
            margin:0;
            padding:0;
            list-style-type:none;
            background-image:url(./imgs/bg.jpg);
            background-size:100% 100%;
            background-attachment:fixed;
            background-repeat:no-repeat;
            
        }
        .loginDiv{
            width:500px;
            height:300px;
            position:fixed;
            left:50%;
            top:50%;
            margin-left:-250px;
            margin-top:-150px;
            border:1px solid #999999;
            box-shadow:0 0 30 #ffffff;
            box-sizing:border-box;
            padding: 0 50px;
            background-color:rgba(0,0,0,0.4);
        }

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="loginDiv">
        <h2 class=" text-center" style="margin-top:20px;color:#ffffff">武汉工程科技学院</h2>
        <h3 class=" text-center" style="margin-top:20px;color:#ffffff">教学管理系统</h3>
        <div class="form-group" style="margin-top:20px">
            <div class="input-group">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-user"></span>
                </div>
                <asp:TextBox runat="server" CssClass="form-control" placeholder="请输入账号" ID="txtUserName"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-lock"></span>
                </div>
                <asp:TextBox runat="server" CssClass="form-control" TextMode="Password"  placeholder="请输入密码" ID="txtUserPassword"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <button type="button" class="btn btn-primary btn-block" id="btnLogin">登陆</button>
        </div>
    </div>
    
    </form>
    <script src="css/bootstrapV3/js/jquery.min.js" type="text/javascript"></script>
    <script src="css/bootstrapV3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/layer/layer.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(function () {
            $("#btnLogin").click(function () {
                var index = layer.load(0, { shade: false }); //0代表加载的风格，支持0-2
                var result = MyAjax.checkLogin($("#txtUserName").val(), $("#txtUserPassword").val());
                var jsonObj = JSON.parse(result.value);
                layer.close(index);
                if (jsonObj.status == "success") {
                    layer.alert("登陆成功", {}, function () {
                        location.href = "AdminIndex.aspx";
                    });
                }
                else {
                    layer.alert("登陆失败");
                }
            })
        })

    </script>
</body>

   
</html>
