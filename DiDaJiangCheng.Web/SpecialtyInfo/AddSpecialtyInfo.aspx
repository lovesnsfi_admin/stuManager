﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddSpecialtyInfo.aspx.cs" Inherits="DiDaJiangCheng.Web.SpecialtyInfo.AddSpecialtyInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="../css/bootstrapV3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../js/layer/theme/default/layer.css" rel="stylesheet" />
    <!--[if lt ie 9]>
        <script src="../css/bootstrapV3/html5shiv.min.js" type="text/javascript"></script>
        <script src="../css/bootstrapV3/respond.min.js" type="text/javascript"></script>
    <![endif]-->
    <title>新增专业</title>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <div class="page-header">
            <h2 class="text-center text-primary">新增专业</h2>
        </div>
        <div role="form" class="form-horizontal">
            <div class="form-group">
                <label class="col-xs-2 control-label">专业名称</label>
                <div class="col-xs-7">
                    <asp:TextBox runat="server" ID="txtSpecialtyName" placeholder="请输入专业名称" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label">专业描述</label>
                <div class="col-xs-7">
                    <asp:TextBox runat="server" ID="txtSpecialtyDescription" CssClass="form-control" TextMode="MultiLine" style="height:150px" placeholder="请输入专业描述"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-7 col-xs-offset-2">
                    <button class="btn btn-primary" type="button" runat="server" id="btn_Save" onserverclick="btn_Save_ServerClick">
                        <span class="glyphicon glyphicon-save"></span>保存
                    </button>
                    <button class="btn btn-warning" type="button" runat="server" id="btnBack" onserverclick="btnBack_ServerClick">
                        <span class="glyphicon glyphicon-arrow-left"></span>返回列表
                    </button>
                </div>
            </div>
        </div>
    </div>
     <asp:HiddenField runat="server" ID="isEdit" Value=""/>
        <asp:HiddenField runat="server" ID="hdSpecialtyId" Value="" />
    </form>
    <script src="../css/bootstrapV3/js/jquery.min.js" type="text/javascript"></script>
    <script src="../css/bootstrapV3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/layer/layer.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#btn_Save").click(function () {
                var txtSpecialtyName = $("#txtSpecialtyName").val();
                var txtSpecialtyDescription = $("#txtSpecialtyDescription").val();
                if (txtSpecialtyName==null||txtSpecialtyName==undefined||txtSpecialtyName=="") {
                    layer.alert("请输入专业名称");

                    return false;
                }
                else if (txtSpecialtyDescription==null||txtSpecialtyDescription==""||txtSpecialtyDescription==undefined) {
                    layer.alert("请输入专业描述");
                    return false;
                }
            })
        })

    </script>
</body>
</html>
