﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SpecialtyInfoList.aspx.cs" Inherits="DiDaJiangCheng.Web.SpecialtyInfo.SpecialtyInfoList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>专业列表</title>
    <link href="../css/bootstrapV3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../js/layer/theme/default/layer.css" rel="stylesheet" />
    <!--[if lt ie 9]>
        <script src="../css/bootstrapV3/html5shiv.min.js" type="text/javascript"></script>
        <script src="../css/bootstrapV3/respond.min.js" type="text/javascript"></script>
    <![endif]-->
</head>
<body>
    <form id="form1" runat="server">
    <div class=" container-fluid">
        <div class="page-header">
            <h2 class="text-center text-primary">专业列表</h2>
        </div>
        <div class="btn-group">
            <button runat="server" id="btn_Add" class="btn btn-primary" onserverclick="btn_Add_ServerClick">
                <span class="glyphicon glyphicon-plus"></span>新增
            </button>
            <button id="btn_Edit" class="btn btn-warning">
                <span class="glyphicon glyphicon-pencil"></span>编辑
            </button>
            <button id="btn_Delete" class="btn btn-danger" runat="server" onserverclick="btn_Delete_ServerClick">
                <span class="glyphicon glyphicon-remove-sign"></span>删除
            </button>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
                <tr>
                    <td>
                        <input type="checkbox" name="ckAll" id="ckAll" />
                    </td>
                    <th class="col-xs-2">专业ID</th>
                    <th class="col-xs-3">专业名称</th>
                    <th>专业描述</th>
                </tr>
                <asp:Repeater runat="server" ID="tableRepeat">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <input type="checkbox" name="ck" value=' <%#Eval("SpecialtyId")%>' />
                            </td>
                            <td><%#Eval("SpecialtyId")%></td>
                            <td><%#Eval("SpecialtyName")%></td>
                            <td><%#Eval("SpecialtyDescription")%></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
        <ul class="pagination pull-right">
            <li class="active">
                <asp:LinkButton Text="1" runat="server"></asp:LinkButton>
            </li>
        </ul>
    </div>
    </form>
     <script src="../css/bootstrapV3/js/jquery.min.js" type="text/javascript"></script>
    <script src="../css/bootstrapV3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../js/layer/layer.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#btn_Edit").click(function () {
                var count = $("input[name='ck']:checked").length;
                console.log(count);
                if (count>1) {
                    layer.alert("一次只能编辑一项")
                }
                else if (count<=0) {
                    layer.alert("请先中你要编辑的项");
                }
                else if (count == 1) {
                    var specialtyId =  $("input[name='ck']")[0].value;
                    var index = layer.open({
                        type: 2,
                        content: "AddSpecialtyInfo.aspx?type=Edit&specialtyId=" + specialtyId
                    });
                    layer.full(index);
                }
                return false;
            });
        })

    </script>
</body>
</html>
