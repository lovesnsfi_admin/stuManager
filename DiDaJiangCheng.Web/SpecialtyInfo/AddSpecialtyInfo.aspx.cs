﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiDaJiangCheng.Web.SpecialtyInfo
{
    public partial class AddSpecialtyInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string type =  Request.QueryString["type"] == null ? "" : Request.QueryString["type"].ToString();
            if (type.ToLower()=="edit")
            {
                //说明是编辑
                this.isEdit.Value = "eidt";
                this.hdSpecialtyId.Value = Request.QueryString["specialtyId"] == null ? "" : Request.QueryString["specialtyId"].ToString();

                this.getModeByspecialtyId(Convert.ToInt32(this.hdSpecialtyId.Value));
            }
        }

        protected void btn_Save_ServerClick(object sender, EventArgs e)
        {
            int i = new BLL.tbSpecialtyInfo().Add(new Model.tbSpecialtyInfo()
            {
                SpecialtyName = this.txtSpecialtyName.Text.Trim(),
                SpecialtyDescription = this.txtSpecialtyDescription.Text.Trim()
            });
            if (i > 0)
            {
                //新增成功
                Common.PageHelper.AlertAndRedirect(this.Page, "新增成功", "SpecialtyInfoList.aspx");
            }
            else 
            {
                Common.PageHelper.Alert(this.Page, "新增失败，请重试或联系管理员");
            }
        }

        protected void btnBack_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("SpecialtyInfoList.aspx");
        }

        protected void getModeByspecialtyId(int specialtyId)
        {
            Model.tbSpecialtyInfo model = new BLL.tbSpecialtyInfo().GetModel(specialtyId);
            this.txtSpecialtyName.Text = model.SpecialtyName;
            this.txtSpecialtyDescription.Text = model.SpecialtyDescription;
        }
    }
}