﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace DiDaJiangCheng.Web.SpecialtyInfo
{
    public partial class SpecialtyInfoList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.getList();
            }
        }

        protected void btn_Add_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("AddSpecialtyInfo.aspx");
        }

        protected void btn_Edit_ServerClick(object sender, EventArgs e)
        {

        }

        protected void btn_Delete_ServerClick(object sender, EventArgs e)
        {

        }
        protected void getList()
        {
           DataSet ds = new BLL.tbSpecialtyInfo().GetAllList();
           this.tableRepeat.DataSource = ds.Tables[0];
           this.tableRepeat.DataBind();
        }
    }
}