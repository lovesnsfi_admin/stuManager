﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminIndex.aspx.cs" Inherits="DiDaJiangCheng.Web.AdminIndex" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="css/bootstrapV3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="js/layer/theme/default/layer.css" rel="stylesheet" />
    <!--[if lt ie 9]>
        <script src="css/bootstrapV3/html5shiv.min.js" type="text/javascript"></script>
        <script src="css/bootstrapV3/respond.min.js" type="text/javascript"></script>
    <![endif]-->
    <title>学生信息管理系统</title>
    <style type="text/css">
        .content{
            position:fixed;
            top:50px;
            bottom:0px;
            width:100%;
            margin:0;
            padding:0;
        }
        .leftMenu{
            height:100%;
            margin:0;
            padding:0;
            border-right:1px solid lightgray;
        }
        .rightContent{
            height:100%;
            margin:0;
            padding:0;
        }
        .panel{
            border-radius:0px;
        }
        .panel-group .panel+.panel{
            margin-top:0;
        }
        .panel-title>a{
            font-weight:bold;
            font-size:18px;
            text-align:center;
            display:block;
            text-decoration:none;
        }
        .panel-body{
            padding:0px;
        }
        .panel-body .list-group{
            margin:0px;
            text-align:center;
        }
        #mainFrame{
            width:100%;
            height:100%;
        }
        .navbar-left-text{
            float:left;
            font-weight:bold;
            margin-left:20px;
            animation:titleAnimate 5s linear infinite;
        }
        @keyframes titleAnimate{
            from{
                color:black;
            }
            30%{
                color:blue;
            }
            60%{
                color:green;
            }
            80%{    
                color:pink;
            }
            to{
                color:black;
            }
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target="#collapse-header">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand" style="padding:0 20px;line-height:50px">
                    <img src="imgs/Logo.png" style="width:50px;height:50px;float:left" />
                    <span class="navbar-left-text">武汉工程科技学院</span> 
                </div>
            </div>
        <div class="collapse navbar-collapse" id="collapse-header">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" ><span class="text-primary">欢迎登陆：杨标</span></a> </li>
                <li>
                    <asp:LinkButton OnClick="logOut_Click" runat="server" ID="logOut"><span id="spanUserName" class="text-danger" style="font-weight:bold" runat="server">退出登陆</span></asp:LinkButton>
                </li>
            </ul>
        </div>
    </div>
    <div class="content row">
        <!--左边的菜单 -->
        <div class="col-sm-2 leftMenu hidden-xs">
            <div class="panel-group" id="menu">
                <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#menu" 
                            href="#speciality">
                            专业管理
                            </a>
                        </h4>
                    </div>
                    <div class="collapse" id="speciality">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="./SpecialtyInfo/AddSpecialtyInfo.aspx" target="mainFrame">新增专业</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="./SpecialtyInfo/SpecialtyInfoList.aspx" target="mainFrame">专页列表</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#menu" 
                            href="#classInfo">
                            班级管理
                            </a>
                        </h4>
                    </div>
                    <div class="collapse" id="classInfo">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="./ClassInfo/AddClassInfo.aspx" target="mainFrame">新增班级</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="./ClassInfo/ClassInfoList.aspx" target="mainFrame">班级列表</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#menu" 
                            href="#studentInfo">
                            学生管理
                            </a>
                        </h4>
                    </div>
                    <div class="collapse" id="studentInfo">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a href="./StudentInfo/AddStudentInfo.aspx" target="mainFrame">新增学生</a>
                                </li>
                                <li class="list-group-item">
                                    <a href="./StudentInfo/StudentInfoList.aspx" target="mainFrame">学生列表</a>
                                </li>
                                <li class="list-group-item">数据分析</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#menu" 
                            href="#cursorInfo">
                            课程管理
                            </a>
                        </h4>
                    </div>
                    <div class="collapse" id="cursorInfo">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">新增课程</li>
                                <li class="list-group-item">课程列表</li>
                                <li class="list-group-item">课程分析</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#menu" 
                            href="#scoreInfo">
                            成绩管理
                            </a>
                        </h4>
                    </div>
                    <div class="collapse" id="scoreInfo">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">成绩录入</li>
                                <li class="list-group-item">成绩列表</li>
                                <li class="list-group-item">成绩分析</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#menu" 
                            href="#systemInfo">
                            系统管理
                            </a>
                        </h4>
                    </div>
                    <div class="collapse" id="systemInfo">
                        <div class="panel-body">
                            <ul class="list-group">
                                <li class="list-group-item">修改密码</li>
                                <li class="list-group-item">重新登陆</li>
                                <li class="list-group-item">意见反馈</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--右边的内容区域 -->
        <div class="col-sm-10 col-xs-12 rightContent">
            <marquee class="text-danger bg-info" style="padding:5px 0;">
                <strong>通知：</strong>
                武汉工程科技学院学生信息管理系统上线测试中....
            </marquee>
            <iframe id="mainFrame" name="mainFrame" frameborder="0"></iframe>
        </div>
    </div>
</nav>
    </form>
    <script src="css/bootstrapV3/js/jquery.min.js" type="text/javascript"></script>
    <script src="css/bootstrapV3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/layer/layer.js" type="text/javascript"></script>
</body>
</html>
