﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace DiDaJiangCheng.Common
{
    public class PageHelper
    {
        public static void Alert(Page page,string msg)
        {
            page.ClientScript.RegisterClientScriptBlock(page.GetType(),"alert","<script>alert('"+msg+"');</script>");
        }

        public static void AlertAndRedirect(Page page, string msg, string url)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<script>alert('{0}');location.href='{1}'</script>", msg, url);
            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "redirect", sb.ToString());
        }
    }
}
